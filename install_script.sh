#!/bin/bash
sudo apt-get install patch xsltproc gcc libreadline-dev python-dev
git clone https://github.com/Chambana/python-mercuryapi.git
cd python-mercuryapi
sudo make PYTHON=python
sudo python setup.py install
sudo usermod -a -G dialout $USER
