# ROS driver for the ThingMagic USB Pro RFID detector
  <br />


## Install Package Dependencies (clone repo and install):
```bash
 cd ~/catkin_ws/src
 git clone https://gitlab.com/chambana/thingmagic_usbpro.git
 ./thingmagic_usbpro/install_script.sh 
 cd ~/catkin_ws
 catkin_make
 source devel/setup.bash
```
<br />


## Launch Package:
```bash
 roslaunch thingmagic_usbpro rfid_detector.launch
```

<br />
## Settings
The following settings can he configured by editing the file **thingmagic_usbpro/launch/rfid_detector.launch**
* Device port -- default is /dev/ttyACM0  
* Device Region code -- default is "NA" for North America  
* Message Publish Rate (max) -- default is 10Hz  


  <br />
  <br />
  <br />

Thanks for [Petr Gotthard](https://github.com/gotthardp) for maintaing the [Mercury API Python Wrapper](https://github.com/gotthardp/python-mercuryapi)
