#!/usr/bin/env python

## Grabs RFID detection data from the ThingMagic USBPro RFID reader and publishes
## thingmagic_usbpro/RFID_Detection messages to the RFID_detections

import rospy
from thingmagic_usbpro.msg import RFID_Detection
import mercury

'''
RFID_Detection message format:
string epc
int8 antenna
int8 read_count
int8 rssi
'''


banner='''
 _____ _     _             __  __             _      
|_   _| |__ (_)_ __   __ _|  \/  | __ _  __ _(_) ___ 
  | | | '_ \| | '_ \ / _` | |\/| |/ _` |/ _` | |/ __|
  | | | | | | | | | | (_| | |  | | (_| | (_| | | (__ 
  |_| |_| |_|_|_| |_|\__, |_|  |_|\__,_|\__, |_|\___|
                     |___/              |___/        
 _   _ ____  ____                    ____  _____ ___ ____  
| | | / ___|| __ ) _ __  _ __ ___   |  _ \|  ___|_ _|  _ \ 
| | | \___ \|  _ \| '_ \| '__/ _ \  | |_) | |_   | || | | |
| |_| |___) | |_) | |_) | | | (_) | |  _ <|  _|  | || |_| |
 \___/|____/|____/| .__/|_|  \___/  |_| \_\_|   |___|____/ 
                  |_|                                      
 ____                _           
|  _ \ ___  __ _  __| | ___ _ __ 
| |_) / _ \/ _` |/ _` |/ _ \ '__|
|  _ <  __/ (_| | (_| |  __/ |   
|_| \_\___|\__,_|\__,_|\___|_|
'''


def RFID_Topic_Publisher():

    pub = rospy.Publisher('RFID_detections', RFID_Detection, queue_size=10)
    rospy.init_node('RFID_node', anonymous=True)
    rospy.loginfo(banner)   

    port_param = rospy.get_param('/rfid_detector_node/port', '/dev/ttyACM0')
    region_param = rospy.get_param('/rfid_detector_node/region', 'NA')
    rate_param = rospy.get_param('/rfid_detector_node/rate', 10)


    rospy.loginfo("Got port name param: "+port_param)
    rospy.loginfo("Got region param: "+region_param)
    rospy.loginfo("Got publishing rate param: "+str(rate_param))

    try:
        reader = mercury.Reader("tmr://" + port_param)
    except Exception as e:
        rospy.loginfo("Couldn't open RFID reader on port: "+str(port_param))
	raise rospy.exceptions.ROSInitException("couldn't open port for RFID reader, check Launch file for port name")
        quit(1)

    try:
        reader.set_region(region_param)
    except Exception as e: 
        rospy.loginfo("Couldn't set RFID reader's region to: "+str(region_param))
        raise rospy.exceptions.ROSInitException("couldn't set region for RFID reader")
        quit(1)

    rate = rospy.Rate(int(rate_param)) #hz

    while not rospy.is_shutdown():
        result = reader.read(5)
        for tagdata in result:
            rospy.loginfo("Antenna:%i Read_Count:%i RSSI:%i EPC:%s" %(tagdata.antenna, tagdata.read_count,tagdata.rssi,tagdata.epc))
            detect_msg = RFID_Detection(str(tagdata.epc),tagdata.antenna,tagdata.read_count,tagdata.rssi)
            pub.publish(detect_msg)
        rate.sleep()

if __name__ == '__main__':
    try:
        RFID_Topic_Publisher()
    except rospy.ROSInterruptException:
        pass

